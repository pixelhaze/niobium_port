//
// niobium.cpp
//
// Copyright (c) 2017 Anup Jayapal Rao <anup.kadam@gmail.com>
//

#include <stddef.h>
#include <stdio.h>

#include <xlist.h>

#include <niobium.h>

dragdropmanager*		dragdropmanager::pOnlyInstance = NULL;

dragdropmanager::dragdropmanager()
{
	m_enableMouseMotionCb = NULL;
	
	m_disableMouseMotionCb = NULL;
	
	m_isEmpty = true;
	
	m_dropSrc = NULL;
	
	m_dropObj = NULL;
}

dragdropmanager::~dragdropmanager()
{
	m_enableMouseMotionCb = NULL;
	
	m_disableMouseMotionCb = NULL;
	
	m_isEmpty = true;
	
	m_dropSrc = NULL;
	
	m_dropObj = NULL;
}

dragdropmanager* dragdropmanager::getInstance()
{
	if (NULL == dragdropmanager::pOnlyInstance)
	{
		dragdropmanager::pOnlyInstance = new dragdropmanager();
	}
		
	return dragdropmanager::pOnlyInstance;
}

void dragdropmanager::attachObject(void* dropSrc, interactable* dropObj)
{
	//print("attach for drop")
	// TODO: Set state of dropObj to indicate it in flight
	//dropObj.show(false)
	
	m_dropSrc = dropSrc;
	m_dropObj = dropObj;
	m_isEmpty = false;
	
	if (NULL != m_enableMouseMotionCb)
	{
		m_enableMouseMotionCb();
	}
}

void dragdropmanager::clearOnDrop()
{
	//print("cleared on drop")
	m_dropSrc = NULL;
	m_dropObj = NULL;
	
	if (NULL != m_disableMouseMotionCb)
	{
		m_disableMouseMotionCb();
	}
}

void dragdropmanager::setPosForDrop(int lx, int ly)
{
	m_dropObj->setPosForDrop(lx, ly);
}

draggable::draggable()
{
	m_bDragAllowed = false;
	m_bDragging = false;
	
	m_dropx = 0;
	m_dropy = 0;
	
	m_draglocal_OffX = 0;
	m_draglocal_OffY = 0;
}

draggable::~draggable()
{
}

bool draggable::isUnderDrag()
{
    return m_bDragging;
}

void draggable::setDropLocation(int mx,int my)
{
	m_dropx = mx - m_draglocal_OffX; 
	m_dropy = my - m_draglocal_OffY;
	
	//printf("set drop location %d %d", m_dropx, m_dropy);
}

bool draggable::startDrag(int mx,int my)
{
	if(true == m_bDragAllowed)
	{
		//printf("start drag");
		
		m_bDragging = true;
		
		m_draglocal_OffX = mx;
		m_draglocal_OffY = my;
			
		setDropLocation(mx, my);
	}
		
	return m_bDragAllowed;
}

bool draggable::stopDrag()
{
	//print("stop drag")
	
	bool bReturn = false;
	
	m_dropx = 0;
	m_dropy = 0;
	
	m_bDragging = false;
	
	if((0 != m_dropx) || (0 != m_dropy))
	{
		bReturn = true;
	}
	
	return bReturn;
}

selectable::selectable()
{
    m_bSelectionAllowed = false;
	m_bSelected = false;
}

selectable::~selectable()
{
}

bool selectable::isSelected()
{
    return m_bSelected;
}

selection::selection(v_fxn_v enableMouseMotionCb, v_fxn_v disableMouseMotionCb)
{
	m_enableMouseMotionCb = enableMouseMotionCb;
	
	m_disableMouseMotionCb = disableMouseMotionCb;
	
	//m_arrItems = [];
}

selection::~selection()
{
    //m_pData = NULL;
    //m_pNext = NULL;
}

void selection::clear()
{
	//print("clearing selection")
	
	interactable* pItem = NULL;
	
	xentry* currEntry = xlist::getIter(&m_arrItems, (void**)&pItem);
    while(NULL != currEntry)
    {	
		pItem->m_selObj.m_bSelected = false;
		
		currEntry = xlist::getNext(currEntry, (void**)&pItem);
    }
		
	//?
	#
	
	m_arrItems.clear();
}

bool selection::isEmpty()
{
	bool bReturn = true;
	
	if(0 < m_arrItems.__len__())
	{
		bReturn = false;
	}
	
	return bReturn;
}
	
void selection::addItem(interactable* pItem)
{
	//if true == item.m_selObj.bSelectionAllowed :
	//print("adding item to selection @",len(m_arrItems))
	m_arrItems.append(pItem);
	
	pItem->m_selObj.m_bSelected = true;
}

void selection::removeItem(interactable* pItem)
{
	//print("removing item from selection")
	m_arrItems.remove(pItem);
	
	pItem->m_selObj.m_bSelected = false;
}

bool selection::containsItem(interactable* pItem)
{
	bool bReturn = false;
	
	if(true == m_arrItems.search((void*)pItem))
	{
		bReturn = true;
	}
	
	return bReturn;
}
	
/*
bool selection::containsPoint(int lx, int ly)
{
	bool bReturn = false;
	
	void* pItem = NULL;
	
	xentry* currEntry = xlist::getIter(&m_arrItems, (void**)&pItem);
    while(NULL != currEntry)
    {	
		if ((lx >= pItem->m_x) && (lx <= pItem->m_x + pItem->m_w ) && (ly >= pItem->m_y) && (ly <= pItem->m_y + pItem->m_h))
		{
			bReturn = bReturn || true;
		}
		
		currEntry = xlist::getNext(currEntry, (void**)&pItem);
    }
	
	return bReturn;
}
*/

void selection::setDropLocation(int mx, int my)
{
	interactable* pItem = NULL;
	
	xentry* currEntry = xlist::getIter(&m_arrItems, (void**)&pItem);
    while(NULL != currEntry)
    {	
		//item.setDropLocation(mx, my);
		interactable* parent = pItem->m_Parent;
		
		if (NULL == parent)
		{
			pItem->setDropLocation(mx, my);
		}
		else
		{
			if(true == m_arrItems.search(parent))
			{
				pItem->setDropLocation(mx, my);	
			}
		}
		
		currEntry = xlist::getNext(currEntry, (void**)&pItem);
    }	
}
	
void selection::startDrag(int mx, int my)
{
	bool bStartDrag = false;
	
	interactable* pItem = NULL;
	
	xentry* currEntry = xlist::getIter(&m_arrItems, (void**)&pItem);
    while(NULL != currEntry)
    {	
		//item.startDrag(mx, my)
		interactable* parent = pItem->m_Parent;
		
		if (NULL == parent)
		{
			bStartDrag = bStartDrag || pItem->startDrag(mx, my);
		}
		else
		{
			if(true == m_arrItems.search(parent))
			{
				bStartDrag = bStartDrag || pItem->startDrag(mx, my);
			}
		}
		
		currEntry = xlist::getNext(currEntry, (void**)&pItem);
    }
	
	if (true == bStartDrag && NULL != m_enableMouseMotionCb)
	{
		m_enableMouseMotionCb();
	}
}
	
bool selection::stopDrag()
{	
	bool bReturn = false;
	
	interactable* pItem = NULL;
	
	xentry* currEntry = xlist::getIter(&m_arrItems, (void**)&pItem);
    while(NULL != currEntry)
    {	
		//bReturn = bReturn || item.stopDrag();
		interactable* parent = pItem->m_Parent;
		
		if (NULL == parent)
		{
			bReturn = bReturn || pItem->stopDrag();
		}
		else
		{
			if(true == m_arrItems.search(parent))
			{
				bReturn = bReturn || pItem->stopDrag();			
			}
		}
		
		currEntry = xlist::getNext(currEntry, (void**)&pItem);
    }
		
	if (NULL != m_disableMouseMotionCb)
	{
		m_disableMouseMotionCb();
	}
	
	return bReturn;
}
	
bool selection::isUnderDrag()
{
	bool bReturn = false;
	
	interactable* pItem = NULL;
	
	xentry* currEntry = xlist::getIter(&m_arrItems, (void**)&pItem);
    while(NULL != currEntry)
    {
		bReturn = bReturn || pItem->isUnderDrag();
		
		currEntry = xlist::getNext(currEntry, (void**)&pItem);
    }
	
	return bReturn;
}

panable::panable(v_fxn_v enableMouseMotionCb, v_fxn_v disableMouseMotionCb)
{
	m_enableMouseMotionCb = enableMouseMotionCb;
	
	m_disableMouseMotionCb = disableMouseMotionCb;
			
	m_bPanning = false;
	
	m_panlocal_OffX = 0;
	
	m_panlocal_OffY = 0;
	
	m_panx = 0;	
	
	m_pany = 0;
}

panable::~panable()
{
}

bool panable::isUnderPan()
{
	return m_bPanning;
}
	
void panable::resetPan(int mx, int my)
{
	m_panlocal_OffX = mx;
	m_panlocal_OffY = my;	
}
	
void panable::startPan(int mx, int my)
{
	//print("pan mode on")	

	m_bPanning = true;
		
	resetPan(mx, my);
	
	m_panx = 0;
	m_pany = 0;
	
	if (NULL != m_enableMouseMotionCb)
	{
		m_enableMouseMotionCb();
	}
}
		
void panable::stopPan()
{
	//print("pan mode off")
	
	m_panlocal_OffX = 0;
	m_panlocal_OffY = 0;
	
	m_panx = 0;
	m_pany = 0;
	
	m_bPanning = false;
	
	if (NULL != m_disableMouseMotionCb)
	{
		m_disableMouseMotionCb();
	}
}
		
void panable::updatePan(int mx, int my, int* ppanx, int* ppany)
{
	if(NULL!=ppanx && NULL!=ppany)
	{
		m_panx = (mx - m_panlocal_OffX);
		
		m_pany = (my - m_panlocal_OffY);	
		
		*ppanx = m_panx;
		
		*ppany = m_pany;
	}
}
	
void panable::getPan_delta(int mx, int my, int* ppanx, int* ppany)
{
	if(NULL!=ppanx && NULL!=ppany)
	{
		int dummy_panx;
		int dummy_pany;
		
		int prev_panx = m_panx;
		int prev_pany = m_pany;
		
		updatePan(mx, my, &dummy_panx, &dummy_pany);
		
		*ppanx = m_panx - prev_panx;
		*ppany = m_pany - prev_pany;
	}
}

trackable::trackable(v_fxn_v enableMouseMotionCb, v_fxn_v disableMouseMotionCb)
{
	m_enableMouseMotionCb = enableMouseMotionCb;
	
	m_disableMouseMotionCb = disableMouseMotionCb;
			
	m_bTracking = false;
	
	m_anchorX = 0;
	
	m_anchorY = 0;
	
	m_trackX = 0;
	
	m_trackY = 0;
}

trackable::~trackable()
{
}
	
bool trackable::isUnderTrack()
{
	return m_bTracking;
}

void trackable::resetTrack(int mx, int my)
{
	m_anchorX = mx;
	
	m_anchorY = my;	
}

void trackable::startTrack(int mx, int my)
{	
	//print("track mode on")	

	m_bTracking = true;
		
	resetTrack(mx, my);
	
	m_trackX = mx;
	
	m_trackY = my;
	
	if (NULL != m_enableMouseMotionCb)
	{
		m_enableMouseMotionCb();
	}
}
		
void trackable::stopTrack()
{
	//print("track mode off")
	
	m_anchorX = 0;
	
	m_anchorY = 0;	
	
	m_trackX = 0;
	
	m_trackY = 0;
	
	m_bTracking = false;
	
	if (NULL != m_disableMouseMotionCb)
	{
		m_disableMouseMotionCb();
	}
}
		
void trackable::updateTrack(int mx, int my)
{
	m_trackX = mx;
	
	m_trackY = my;
}

zoomable::zoomable()
{
	m_bZooming = false;
			
	m_zoomRESET = 1.0;
	
	m_zoomMIN = 1.0 / 1024;
	
	m_zoomMAX = 1.0 * 1024;
	
	m_bZoomH = true;
	
	m_bZoomV = true;
}

zoomable::~zoomable()
{
}

bool zoomable::isUnderZoom()
{
	return m_bZooming;
}

void zoomable::recalculateVirtualSize(int w, int h, int* pnw, int* pnh)
{
	if(NULL!=pnw && NULL!=pnh)
	{
		*pnw = w;
		
		*pnh = h;
		
		if (true == m_bZoomV)
		{
			*pnh = h/m_zoomlevel;		
		}
		
		if (true == m_bZoomH)
		{
			*pnw = w/m_zoomlevel;
		}
	}
}
	
void zoomable::resetZoomlevel()
{
	//printf("[1]\n")
	m_zoomlevel = m_zoomRESET;
	
	m_prev_zoomlevel = m_zoomlevel;	
}

void zoomable::normalizeZoomlevel()
{
	if (m_zoomlevel > m_zoomMAX)
	{
		m_zoomlevel = m_zoomMAX;
	}
		
	if (m_zoomlevel < m_zoomMIN)
	{
		m_zoomlevel = m_zoomMIN;
	}
}
	
void zoomable::increaseZoomlevel()
{
	//print("[+]\n")
	m_prev_zoomlevel = m_zoomlevel;
	
	if (m_zoomlevel >= m_zoomMIN && m_zoomlevel < m_zoomMAX)
	{
		m_zoomlevel = m_zoomlevel * ZOOM_FACTOR;
	}
	
	normalizeZoomlevel();
}

void zoomable::decreaseZoomlevel()
{
	//print("[-]\n")
	m_prev_zoomlevel = m_zoomlevel;
	
	if (m_zoomlevel > m_zoomMIN && m_zoomlevel <= m_zoomMAX)
	{
		m_zoomlevel = m_zoomlevel / ZOOM_FACTOR;
	}
			
	normalizeZoomlevel();
}

interactable::interactable()
{
	m_Parent = NULL;
	
	//
	
	m_dragpad_OffX = 0;
	
	m_dragpad_OffY = 0;
	
	m_dropX = 0;
	
	m_dropY = 0;
	
	//
	
	m_selObj = selectable();
	
	//
	
	m_dragObj = draggable();
}

interactable::~interactable()
{
	
}

void interactable::setPosForDrop(int lx, int ly)
{
	m_dropX = lx - m_dragpad_OffX;
	m_dropY = ly - m_dragpad_OffY;
}

bool interactable::startDrag(int mx, int my)
{
	return m_dragObj.startDrag(mx, my);
}
	
bool interactable::isUnderDrag()
{
	return m_dragObj.isUnderDrag();
}
	
void interactable::setDropLocation(int mx, int my)
{
	m_dragObj.setDropLocation(mx, my);
}

bool interactable::stopDrag()
{		
	bool bReturn = m_dragObj.stopDrag();
	
	return bReturn;
}
