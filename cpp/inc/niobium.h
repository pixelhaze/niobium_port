//
// niobium.h
//
// Copyright (c) 2017 Anup Jayapal Rao <anup.kadam@gmail.com>
//

#ifndef NIOBIUM_H
#define NIOBIUM_H

#ifdef  cplusplus
extern "C" {
#endif

#include <xlist.h>

#define ZOOM_FACTOR	1.2
typedef void (*v_fxn_v)();

class interactable;

class dragdropmanager
{

    public:

        dragdropmanager();

        ~dragdropmanager();
        
        void attachObject(void*, interactable*);
        
        void clearOnDrop();
        
        void setPosForDrop(int, int);
        
        static dragdropmanager* getInstance();
        
		v_fxn_v		m_enableMouseMotionCb;
		
		v_fxn_v		m_disableMouseMotionCb;

		bool						m_isEmpty;
		
		void*						m_dropSrc;
		
		interactable*				m_dropObj;     
		
		static dragdropmanager*		pOnlyInstance;   

    //protected:

    //private:
};

class draggable
{

    public:

        draggable();

        ~draggable();
        
        bool isUnderDrag();
        
        void setDropLocation(int, int);
        
        bool startDrag(int, int);
        
        bool stopDrag();
        
		bool m_bDragAllowed;
		bool m_bDragging;
		
		int m_dropx;
		int m_dropy;
		
		int m_draglocal_OffX;
		int m_draglocal_OffY;        

    //protected:

    //private:
};

class selectable
{

    public:

        selectable();

        ~selectable();
        
        bool isSelected();
        
        bool		m_bSelectionAllowed;
        
        bool		m_bSelected;

    //protected:

    //private:
};

class selection
{

    public:

        selection(v_fxn_v enableMouseMotionCb=NULL, v_fxn_v disableMouseMotionCb=NULL);

        ~selection();

        void clear();
        
        bool isEmpty();
        
        void addItem(interactable* item);
        
        void removeItem(interactable* item);
        
        bool containsItem(interactable* item);
        
        //bool containsPoint(int lx, int ly);
        
        void setDropLocation(int, int);
        
        void startDrag(int, int);
        
        bool stopDrag();
        
        bool isUnderDrag();
        
		v_fxn_v		m_enableMouseMotionCb;
		
		v_fxn_v		m_disableMouseMotionCb;
		
		void*		m_parent;
		
		xlist		m_arrItems;        
        
    //protected:

    //private:
};

class panable
{

    public:

        panable(v_fxn_v enableMouseMotionCb=NULL, v_fxn_v disableMouseMotionCb=NULL);

        ~panable();
        
        bool isUnderPan();
        
        void resetPan(int, int);
        
        void startPan(int, int);
        
        void stopPan();
        
        void updatePan(int, int, int*, int*);
        
        void getPan_delta(int, int, int*, int*);
        
		v_fxn_v		m_enableMouseMotionCb;
		
		v_fxn_v		m_disableMouseMotionCb;
		
		bool		m_bPanning;
		
		int			m_panlocal_OffX;
		
		int			m_panlocal_OffY;
		
		int			m_panx;	
		
		int			m_pany;		        

    //protected:

    //private:
};

class trackable
{
    public:

        trackable(v_fxn_v enableMouseMotionCb=NULL, v_fxn_v disableMouseMotionCb=NULL);

        ~trackable();
        
        bool isUnderTrack();
        
        void resetTrack(int, int);
        
        void startTrack(int, int);
        
        void stopTrack();
        
        void updateTrack(int, int);
        
		v_fxn_v		m_enableMouseMotionCb;
		
		v_fxn_v		m_disableMouseMotionCb;
				
		bool 		m_bTracking;
		
		int 		m_anchorX;
		
		int 		m_anchorY;
		
		int 		m_trackX;
		
		int 		m_trackY;        
        
    //protected:

    //private:
};


class zoomable
{

    public:

        zoomable();

        ~zoomable();
        
        bool isUnderZoom();
        
        void recalculateVirtualSize(int, int, int*, int*);
        
        void resetZoomlevel();
        
        void normalizeZoomlevel();
        
        void increaseZoomlevel();
        
        void decreaseZoomlevel();
        
		bool m_bZooming;
				
		double m_zoomRESET;
		
		double m_zoomMIN;
		
		double m_zoomMAX;
		
		double m_zoomlevel;
		
		double m_prev_zoomlevel;		
		
		bool m_bZoomH;
		
		bool m_bZoomV;        

    //protected:

    //private:
};

class interactable
{
    public:

        interactable();

        ~interactable();
        
        void setPosForDrop(int, int);
        
        bool startDrag(int , int);
        
        bool isUnderDrag();
        
        void setDropLocation(int , int);
        
        bool stopDrag();
        
        //
        
        interactable*	m_Parent;
        
        //
        	
        int				m_dragpad_OffX;
		
		int				m_dragpad_OffY;
		
		int				m_dropX ;
		
		int				m_dropY;
		
		//
		
		selectable		m_selObj;
		
		//
		
		draggable		m_dragObj;
		
    //protected:

    //private:	
};

#ifdef  cplusplus
};
#endif

#endif
